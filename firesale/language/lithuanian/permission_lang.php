<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['firesale.role_edit_orders']                = 'Edit Orders'; # Translate
$lang['firesale.role_access_gateways']            = 'Access Gateways'; # Translate
$lang['firesale.role_install_uninstall_gateways'] = 'Install/Uninstall Gateways'; # Translate
$lang['firesale.role_enable_disable_gateways']    = 'Enable/Disable Gateways'; # Translate
$lang['firesale.role_edit_gateways']              = 'Edit Gateways'; # Translate
$lang['firesale.role_access_routes']              = 'Access Routes'; # Translate
$lang['firesale.role_create_edit_routes']         = 'Create/Edit Routes'; # Translate
$lang['firesale.role_access_currency']            = 'Access Currency'; # Translate
$lang['firesale.role_install_uninstall_currency'] = 'Install/Uninstall Currency'; # Translate